sort.h 
======
Overview
--------

'Sort.h' is collection of many sorting algorithms in C Language. The overhead of the using SLR is not needed. It is an optmizited implementation of sorting algorithms.

You also do not need to link to the library, whole program is contained in the header files. 

You can do the following sorting using the code:

* Shellsort - You will get this after combining insertion and bubble sort.
* Heapsort  - When you improved the insertions sort, you will get the heapsort. 
* Quicksort - You will get the quicksort by combining.
* Merge sort - Divide and conquer algorithm explaination can be found on this page
* In-place merge sort (*not* stable)
* Selection sort - Based on selection of elements
* Other sorting algorithms

Sorting is not only simple and has many variations compared wih all the other algorithms available. This all started with <a href="http://www.simplyswitch.com">simple concept</a> of arranging and today we have more than 1000 algorithms.